public class Square extends Shape{
    private double width;

    public Square(String name, double width) {
	super(name);
	this.width = width;
    }

    public double getArea(){
	return Math.pow(width, 2);
    }

    public double getPerimeter(){
	return 4*width;
	return 2*width;
    }
}
