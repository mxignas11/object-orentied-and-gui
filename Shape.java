public abstract class Shape{
    private String name; // Creating a name for the object

    // constructor
    public Shape(String name){
	this.name = name;
    }

    public abstract double getArea();
    public abstract double getPerimeter();

    public String toString(){
	return "Shape name: " + name + " Area: " + getArea() + " Perimeter: " + getPerimeter();
    }
}
