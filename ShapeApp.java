import java.util.ArrayList;

public class ShapeApp{
    public static void main(String args[]){
	ArrayList<Shape> shapes = new ArrayList<Shape>();
	shapes.add(new Circle("Small Circle", 5));
	shapes.add(new Circle("Medium Circle", 20));
	shapes.add(new Square("Small Square", 5));

	for (Shape shape : shapes)
	    System.out.println(shape);
    }
}
